console.log('background goes');

let messageIterator = 0;
let siteArray = [];
//let siteNameArray = [];

let getNameDomainMessage = function(){
	let sites = new XMLHttpRequest();
	//let siteArray = [];
	let simplePromise = new Promise((resolve, reject)=>{
		sites.open("GET", "http://www.softomate.net/ext/employees/list.json");
 		sites.addEventListener("load", function(data){
			resolve(this.responseText)
		});
		sites.send();
	});

	simplePromise.then(result => {
		siteArray = JSON.parse(result);
		console.log('siteNameArray ', getSiteNameArray());
		console.log('siteDomainArray ', getSiteDomainArray());
		//console.log('result from promise: ', siteArray);
		//sendResponse({siteArray: siteArray});
	});
}

let getSiteNameArray = function(){
	let siteNameArray = [];
	siteArray.forEach(function(value, index){
		siteNameArray.push(value.name);
	});
	//console.log('siteNameArray ', siteNameArray);
	return siteNameArray;
}

let getSiteDomainArray = function(){
	let siteDomainArray = [];
	siteArray.forEach(function(value, index){
		siteDomainArray.push(value.domain);
	});
	//console.log('siteNameArray ', siteNameArray);
	return siteDomainArray;
}

let checkCoincidenses = function(currentUrl){
	let coincidence;
	getSiteDomainArray().forEach(function(value, index){
		if(currentUrl.includes(value)){
			coincidence = value;
		}
	})
	return coincidence;
}

let takeIconURL = function(){
	let imgURL = chrome.runtime.getURL("img/icon16.png");
	return imgURL;
}

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
	if(request.todo == "takeMessageIterator"){
		sendResponse({messageIterator: messageIterator});
		console.log('is sent to content');
	}
});

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
	if(request.todo == "reloadMessageIterator"){
		console.log('got iterator from content after reload, equals ', request.iterator);
		messageIterator = request.iterator;
		if(messageIterator >= 4){
			console.log('the number of messages is reached, restrictMessage is sent');
			sendResponse({message: 'restrictMessage'});
		}
		else{
			sendResponse({message: 'allowMessage'});
		}
	}
});

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
	if(request.todo == "restrictMessages"){
		console.log('the "X" button was pressed ');
		messageIterator = 4;
	}
});

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
	if(request.todo == "showPageAction"){
		//console.log('Should be the corresponding domain of the current page ', checkCoincidenses(request.url));
		//console.log(request.url, sendResponse);
		//sendResponse({imgUrl: takeIconURL(), domain: value});
		if(request.url.includes('google.com')){
			console.log('There is google');
			//let imgURL = chrome.runtime.getURL("img/icon16.png");
			sendResponse({imgUrl: takeIconURL(), domain: "google.com"});
		}
		else if(request.url.includes('google.ru')){
			//let imgURL = chrome.runtime.getURL("img/icon16.png");
			sendResponse({imgUrl: takeIconURL(), domain: "google.ru"});
		}
		else if(request.url.includes('bing')){
			console.log('There is bing');
			//let imgURL = chrome.runtime.getURL("img/icon16.png");
			sendResponse({imgUrl: takeIconURL(), domain: "bing"});
		}
		// else if(request.url.includes('yandex')){
		// 	console.log('There is yandex');
		// 	//let imgURL = chrome.runtime.getURL("img/icon16.png");
		// 	sendResponse({imgUrl: takeIconURL(), domain: "yandex"});
		// }
		else{
			console.log('No matches found');
		}
	} 
	else if(request.todo == "getSiteArray"){
		sendResponse({siteArray: siteArray})
	}
	else if(request.todo == "showPageMessage"){
		sendResponse({currentDomain: checkCoincidenses(request.url)})
	}
	return true;
});

//getNameDomainMessage();

//let date = new Date();
//console.log('date ' , date);
//let msec = date.getMilliseconds();
// let msec = Date.now();
// console.log('date taken');
// console.log('msec '. msec);

// let date = Date.parse(new Date());
// console.log('date equals ', date);

//setInterval(function(){ 

	chrome.storage.sync.get ('lastDate', function(obj){

				let date = Date.parse(new Date());
				if(obj.lastDate){
					getNameDomainMessage();
					setInterval(function(){ //?????????????????????????????? 
						chrome.storage.sync.set({'lastDate': date}, function(){
							console.log('lastDate is updated ');
						});
					}, 3600000);
					//insert set interval of 120000
					console.log('lastDate exists ', obj.lastDate);
					console.log('newDate equals ', date);
					//let gap = date - obj.lastDate;
					//console.log('gap equals ', gap);
					//newDate = date;
					// if(gap >= 120000){ //two minutes
					// 	getNameDomainMessage();
					// 	console.log('the gap is big enough to update the siteList');
					// 	chrome.storage.sync.set({'lastDate': date}, function(){
					// 		console.log('lastDate is updated ');
					// 	});
					// }
					//compare dates, lastDate and date
				}
				else{
					console.log('lastDate does not exist');
					chrome.storage.sync.set({'lastDate': date}, function(){
						console.log('lastDate is sent into storage ');
					});
					getNameDomainMessage();
					//just run getNameDomainMessage();
				}
				
	});
//}, 6000);

