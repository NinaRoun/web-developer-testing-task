console.log('content script goess');
let messageState, currentMatch, siteList, messageToInsert;

// chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {

//      let activeTab = tabs[0];
//      let activeTabId = activeTab.id;
//      console.log(activeTab.url);

// });

// chrome.tabs.getSelected(null, function(tab){
// 	let tabUrl = tab.url;
// 	console.log("Taburl" , tabUrl);
// });

let getImgUrl = function(response){
	//console.log('in getImgUrl')
	let template = Handlebars.templates['getImage'];
	let imgData = template({
		imgUrl: response.imgUrl
	});
	//console.log('in getImgUrl, imgData = ', imgData)
	return imgData;
};

let setIconImg = function(iconPlace, urlContainer, commonParent, url){
	//console.log('in setIconImg')
	chrome.runtime.sendMessage({todo: "getSiteArray"}, function(domainsResponse){
		siteList = domainsResponse.siteArray;
		let domainsArray = [];
		//console.log('response from getDomains', domainsResponse.siteArray[0].domain);
		for(elem of siteList){
			//console.log('elem', elem)
			domainsArray.push(elem.domain);
		}
		//console.log(iconPlace)
			$(iconPlace).each(function(index, element){
				//console.log('index = ', index, element)
				//const resultUrl = $(elem2).eq(index).text();
				const resultUrl = $(element).closest(commonParent).find(urlContainer).text();
				const targetDiv = $(element);
				//console.log(resultUrl);
				domainsArray.forEach(function(value, index){
					//console.log('we are in domainsArray.forEach')
					if(resultUrl.includes(value)){
						//console.log("you've got some matches in: ", resultUrl);
						targetDiv.prepend(url);
						//console.log('prepended')
					}
				})
			})

	});
}

// let getMessageData = function(response){
// 	console.log('in getMessageData')
// 	let template = Handlebars.templates['getMessage'];
// 	let messageData = template({
// 		message: response.message//change
// 	});
// 	console.log('in getImgUrl, imgData = ', messageData)
// 	return messageData;
// };

let defineMessageByName = function(name, arrayOfSites){
	let coincidence;
	arrayOfSites.forEach(function(value, index){
		if(value.name.includes(name)){
			coincidence = value.message;
		}
	})
	return coincidence;
}

let hideMessageData = function(){
	//$('#closeMessageContainerButton').click(function(){
		//console.log('X was pressed!!!')
		$('#messageContainer').hide(1000);
		chrome.runtime.sendMessage({todo: "restrictMessages"}, function(response){
		});
	//});
}

let addStyleToPages = function(url){	
	let elem = document.createElement('link');
	elem.rel = 'stylesheet';
	elem.setAttribute('href', url);
	document.body.appendChild(elem);
}

let insertMessageDataToPageGoogle = function(place, messageData){
	$(place).prepend(messageData);
	// $('div#resultStats').css({'margin-top': '30px'});
	// $('#messageContainer').css({'display': 'flex', 'height': '25px', 'width': '632px', 'background-color': '#F0E68C', 'margin-bottom': '20px', 'margin-top': '20px', 'border-radius': '8px', 'margin-left': '150px'});
	// $('#messageContainerText').css({'margin': 'auto', 'text-align': 'center'});
	// $('#closeMessageContainerButton').css({'float': 'right', 'width': '25px', 'text-align': 'center', 'margin': 'auto', 'cursor': 'pointer'});
	//console.log('before injecting style');
	let cssUrl = chrome.runtime.getURL('css/content/forGoogle.css')
	addStyleToPages(cssUrl);
	debugger;
	//console.log('styles added!')

	$('#closeMessageContainerButton').click(function() {
		hideMessageData();
		$('div#resultStats').css({'margin-top': '0px'});
	});
	// $('#closeMessageContainerButton').click(function(){
	// 	//console.log('X was pressed!!!')
	// 	$('#messageContainer').hide(1000);
	// 	$('div#resultStats').css({'margin-top': '0px'});
	// });
}

let insertMessageDataToPageBing = function(place, messageData){
	console.log('in insertMessageDataToPageBing');
	$(place).prepend(messageData);
	// $('#messageContainer').css({'display': 'flex', 'height': '25px', 'width': '632px', 'background-color': '#F0E68C', 'margin-bottom': '20px', 'margin-top': '20px', 'border-radius': '8px', 'margin-left': '150px'});
	// $('#messageContainerText').css({'margin': 'auto', 'text-align': 'center'});
	// $('#closeMessageContainerButton').css({'float': 'right', 'width': '25px', 'text-align': 'center', 'margin': 'auto', 'cursor': 'pointer'});
	// console.log('prepended, css for message is ready');
	let cssUrl = chrome.runtime.getURL('css/content/forBing.css')
	addStyleToPages(cssUrl);
	$('#closeMessageContainerButton').click(hideMessageData);
	// $('#closeMessageContainerButton').click(function(){
	// 	//console.log('X was pressed!!!')
	// 	$('#messageContainer').hide(1000);
	// });
}

let insertMessageDataToPageYandex = function(place, messageData){
	console.log('in insertMessageDataToPageYandex');
	$(place).append(messageData);
	// $('#messageContainer').css({'display': 'flex', 'height': '25px', 'width': '632px', 'background-color': '#F0E68C', 'margin-bottom': '20px', 'margin-top': '20px', 'border-radius': '8px'});
	// $('#messageContainerText').css({'margin': 'auto', 'text-align': 'center', 'line-height': '1em'});
	// $('#closeMessageContainerButton').css({'float': 'right', 'width': '25px', 'text-align': 'center', 'margin': 'auto', 'cursor': 'pointer', 'line-height': '1em'});
	// console.log('prepended, css for message is ready');
	let cssUrl = chrome.runtime.getURL('css/content/forYandex.css')
	addStyleToPages(cssUrl);
	$('#closeMessageContainerButton').click(hideMessageData);
	// $('#closeMessageContainerButton').click(function(){
	// 	//console.log('X was pressed!!!')
	// 	$('#messageContainer').hide(1000);
	// });
}

let getMessageIterator = function(){
	console.log('in getMessageIterator');
	// let sendTakeMessageIterator = function(){
	// 	chrome.runtime.sendMessage({todo: "takeMessageIterator"}, function(response){
	// 		console.log('iterator is taken and is ', response.messageIterator + 1);
	// 		response.messageIterator++;
	// 		//console.log('messageIterator is uploaded');
	// 		resolve(response.messageIterator);
	// 	});
	// }
	const iteratorPromise = new Promise((resolve, reject)=>{
  		chrome.runtime.sendMessage({todo: "takeMessageIterator"}, function(response){
			//let url = location.host;
			if(!location.host.includes('google.com')){
				console.log('iterator is taken and is ', response.messageIterator + 1);
				response.messageIterator++;
				//console.log('messageIterator is uploaded');
				resolve(response.messageIterator);
			}
		});
	});

	iteratorPromise.then(result => {
  		console.log('result from promise ' + result);
  		getMessageState(result);
	});

	//console.log('result from step 1 - iterator - ', sendTakeMessageIterator());
}

let injectMessage = function(state, match, sites){
	console.log('in injectMessage');
	sites.forEach(function(value, index){
		if(value.domain == match){
			messageToInsert = value.message;
			//setMessageTemplate(messageToInsert);
			if(value.domain == 'google.ru' && state === 'allowMessage'){
				//setMessageTemplate('div#topabar', messageToInsert); //check name in function, remove
				insertMessageDataToPageGoogle('div#topabar', setMessageTemplate(messageToInsert));
			}
			else if((value.domain == 'bing.com' && state === 'allowMessage')){
				insertMessageDataToPageBing('div#b_content', setMessageTemplate(messageToInsert));
			}
			else if((value.domain == 'yandex.ru' && state === 'allowMessage')){
				insertMessageDataToPageYandex('div.Zk.headline', setMessageTemplate(messageToInsert));
			}
			else{
				console.log('message is not injected');
			}
		}
	})
}

let checkCurrentUrlIncludesAnyDomain = function(resultOfMessageIterator){
	messageState = resultOfMessageIterator;
	console.log('resultOfMessageIterator in someFunc ', resultOfMessageIterator); //allowMessage
	const iteratorPromise = new Promise((resolve, reject)=>{
		let url = location.host;
  		chrome.runtime.sendMessage({todo: "showPageMessage", url: url}, function(response){
			console.log('result from background (should be current domain if there are matches) ', response.currentDomain);
			//getMessageIterator();
			resolve(response.currentDomain); //e.g. bing.com
		});
	});

	iteratorPromise.then(result => {
		currentMatch = result;
		//let messageCounter;
  		console.log('result from promise of getting current domain coincedence ' + messageState);
  		console.log('currentMatch (domain) ', currentMatch);
  		console.log('siteList with objects ', siteList);
  		injectMessage(messageState, currentMatch, siteList);
	});

}

let getMessageState = function(iterator){
	// console.log('first - getMessageState');
	// let messageState;
	// chrome.runtime.sendMessage({todo: "reloadMessageIterator", iterator: iterator}, function(response){
	// 	messageState = response.message;
	// 	//console.log('messageEsceed ', messageEsceed);
	// 	//return messageEsceed;
	// });
	// console.log('result from step 2 - messageState - ', messageState);
	// return messageState;
	const messageStatePromise = new Promise((resolve, reject)=>{
  		chrome.runtime.sendMessage({todo: "reloadMessageIterator", iterator: iterator}, function(response){
			//messageState = response.message;
			resolve(response.message);
		});
	});

	messageStatePromise.then(result => {
  		//console.log('result from messageStatepromise ' + result); //allow message or restrict
  		//getGeneralSiteData();
  		checkCurrentUrlIncludesAnyDomain(result);
  		//console.log('trying to run someFunc');
	});
}

let setMessageTemplate = function(message){
	console.log('in setMessageTemplate')
	//chrome.runtime.sendMessage({todo: "getSiteArray"}, function(domainsResponse){
		// const messageArray = [];
		// //console.log('response from getDomains', domainsResponse.siteArray[0].domain);
		// for(elem of domainsResponse.siteArray){
		// 	//console.log('elem', elem)
		// 	messageArray.push(elem.message);
		// }
		//console.log('got messages from response', messageArray) //successfully: array-3 elem
		//console.log(iconPlace)  

		let template = Handlebars.templates['getMessage'];
		let messageData = template({
			//message: messageArray[0]
			//message: defineMessageByName(name, domainsResponse.siteArray)
			message: message
		});
		//console.log('Corresponding message ', defineMessageByName(name, domainsResponse.siteArray)); //successfuly
		//console.log('result from template', messageData) //to insert

		//insert messageData to div#topabar (for google)
		//console.log($('div#topabar', 'div#topabar'));
		// if(name === 'google.ru'){
		// 	//console.log('data for google inserted')
		// 	insertMessageDataToPageGoogle(place, messageData);
		// }
		// else if(name === 'bing'){
		// 	//console.log('data for bing inserted')
		// 	insertMessageDataToPageBing(place, messageData);
		// }
		// else if(name === 'yandex'){
		// 	console.log('data for yandex inserted')
		// 	insertMessageDataToPageYandex(place, messageData);
		// }
		// else{
		// 	console.log('data failed to be inserted as there are no matches');
		// }

	//});
	return messageData;
}

let getDataToInjectIcon = function(){
	console.log('in getGeneralSiteData, start defining domain')
	let url = location.host;
	let messageCounter;
	chrome.runtime.sendMessage({todo: "showPageAction", url: url}, function(response){
		//console.log('presumable callback', response)
		//console.log(response.domain);
		if(response.domain == "google.ru" || response.domain == "google.com"){
			let name = response.domain;
			console.log("google catched");
			//console.log('go to setIconImg')
			//to insert icons
			let url = getImgUrl(response);

			setIconImg('div.r', '.iUh30', 'div.rc', url);

			//to insert message
			// let message = getMessageData(response);
			// if(name == "google.ru"){
			// 	if(resultOfMessageIterator === 'allowMessage'){
			// 		setMessageTemplate('div#topabar', name);
			// 	}
			// 	else{
			// 		console.log('message is restricted');
			// 	}
			// }

			//document.getElementById("topabar").src = imgURL;
			//$(".r").css('background-color',"red");
			//console.log('response contains ', response)

			// let template = Handlebars.templates['getImage'];
			// let imgData = template({
			// 	imgUrl: response.imgUrl
			// });
			//console.log('response.imgUrl ', response.imgUrl)

			// console.log("Before inserting", $('.r').html());
			// $('.r').html() = $('.r').html(imgData) + $('.r').html();
			// console.log("After inserting", $('.r').html());
			// $('.r').each(function(){
			// 	//console.log($('.iUh30'));
			// 	$('.iUh30').each
			// 	$(this).html(imgData + $(this).html())
			// })

			// chrome.runtime.sendMessage({todo: "getDomains"}, function(domainsResponse){
			// 	const domainsArray = [];
			// 	//console.log('response from getDomains', domainsResponse.siteArray[0].domain);
			// 	for(elem of domainsResponse.siteArray){
			// 		//console.log('elem', elem)
			// 		domainsArray.push(elem.domain);
			// 	}

			// 	$('div.r').each(function(index, element){
			// 		const resultUrl = $('.iUh30').eq(index).text();
			// 		const targetDiv = $(element);
			// 		//console.log(resultUrl);
			// 		domainsArray.forEach(function(value, index){
			// 			//console.log('we are in domainsArray.forEach')
			// 			if(resultUrl.includes(value)){
			// 				console.log("you've got some matches in: ", resultUrl);
			// 				targetDiv.prepend(getImgUrl(response));
			// 				console.log('prepended')
			// 			}
			// 		})
			// 	})

			// });

			//console.log('final domainsArray', domainsArray);

		}
		else if(response.domain == "bing"){
			console.log("bing catched");
			let url = getImgUrl(response);
			setIconImg('div.b_title', 'cite', '.b_algo', url);

			// let name = response.domain;
			// if(resultOfMessageIterator === 'allowMessage'){
			// 	setMessageTemplate('div#b_content', name);
			// }
			// else{
			// 	console.log('message is restricted');
			// }
		}
		//else if(response.domain == "yandex"){
			//console.log("yandex catched");

			// let name = response.domain;
			// if(resultOfMessageIterator === 'allowMessage'){
			// 	setMessageTemplate('div.Zk.headline', name);
			// }
			// else{
			// 	console.log('message is restricted');
			// }
		//}
		else{
			console.log('No matches found');
		}
	});
}

let getSiteList = function(){
	chrome.runtime.sendMessage({todo: "getSiteArray"}, function(domainsResponse){
		siteList = domainsResponse.siteArray;
		console.log('in getSiteList, siteList is ', siteList);
	});
}

let runMainPoint = function(){
	console.log('step 0 - runMainPoint is run');
	const iteratorThenMessagePromise = new Promise((resolve, reject)=>{
		getSiteList();
		getMessageIterator();
		//someFunc(); //responsible for injecting messages
		getDataToInjectIcon(); //responsible for injecting icons
		//getMessageIterator();
  		//console.log('getMessageState is finished, promise resolve = (some message)', getMessageState());
  		//resolve(getMessageState());  //returns messageState: allow or restrict
	});

	iteratorThenMessagePromise.then(result => {
		//someFunc();
		//console.log('later');
  		//console.log('result from promise: (some message)' + result);
  		//getGeneralSiteData(result);
	});
}

runMainPoint();




