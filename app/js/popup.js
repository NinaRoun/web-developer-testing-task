$(function(){


let sites = new XMLHttpRequest();
let siteArray = [];
let simplePromise = new Promise((resolve, reject)=>{
	sites.open("GET", "http://www.softomate.net/ext/employees/list.json");
 	sites.addEventListener("load", function(data){
		resolve(this.responseText)
	});
	sites.send();
});

simplePromise.then(result => {
	let siteArray = JSON.parse(result)
	//console.log('result from promise: ', siteArray);
 
	let template = Handlebars.templates['getlist'];
	let siteData = template({
		sites: [
			{site: siteArray[0].name},
			{site: siteArray[1].name},
			{site: siteArray[2].name}
      		]
		});
	$('#list').html(siteData);

});

// chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
// 	chrome.tabs.sendMessage(tabs[0].id, {todo: "putIcon"})
// });

});