let gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglifyjs'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    cache = require('gulp-cache'),
    autoprefixer = require('gulp-autoprefixer'),
    handlebars = require('gulp-handlebars'),
    wrap = require('gulp-wrap'),
    declare = require('gulp-declare');

gulp.task('sass-popup', function(){
  return gulp.src('app/sass/popup/*.sass')
  .pipe(sass())
  .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
  .pipe(gulp.dest('app/css/popup'))
  .pipe(browserSync.reload({stream: true}))
});

gulp.task('sass-content', function(){
  return gulp.src('app/sass/content/*.sass')
  .pipe(sass())
  .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
  .pipe(gulp.dest('app/css/content'))
  .pipe(browserSync.reload({stream: true}))
});

gulp.task('templates', function(){
  gulp.src('app/templates/*.hbs')
    .pipe(handlebars())
    .pipe(wrap('Handlebars.template(<%= contents %>)'))
    .pipe(declare({
       namespace: 'Handlebars.templates',
       noRedeclare: true, // Avoid duplicate declarations
    }))
    .pipe(concat('templates.js'))
    .pipe(gulp.dest('dist/js/'));
});

gulp.task('browser-sync', function(){
  browserSync({
    server: {
      baseDir: 'app'
    },
    notify: false,
    open: 'external',
    browser: 'chrome'
  });
});

gulp.task('scripts', function(){
  return gulp.src([
      'app/libs/jquery/dist/jquery.min.js'
    ])
  .pipe(concat('libs.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('app/js'));
});

gulp.task('css-libs', ['sass-popup', 'sass-content'], function(){
  return gulp.src('app/css/popup/libs.css')
  .pipe(cssnano())
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest('app/css'));
});

gulp.task('clean', function(){
  return del.sync('dist');
});

gulp.task('clear', function(){
  return cache.clearAll();
});

gulp.task('img', function(){
  return gulp.src('app/img/**/*')
  .pipe(cache(imagemin({
    interlaced: true,
    progressive: true,
    svgoPlugins: [{removeViewBox: false}],
    une: [pngquant()]
  })))
  .pipe(gulp.dest('dist/img'));
});

gulp.task('watch', ['browser-sync', 'css-libs', 'scripts'], function(){
  gulp.watch('app/sass/**/*.sass', ['sass-popup', 'sass-content']);
  gulp.watch('app/*.html', browserSync.reload);
  gulp.watch('app/js/**/*.js', browserSync.reload);
});

gulp.task('build', ['clean', 'img', 'sass-popup', 'sass-content', 'scripts', 'templates'], function(){
  let buildCssForPopup = gulp.src([
    'app/css/popup/*.css',
    'app/css/libs.min.js'
    ])
  .pipe(gulp.dest('dist/css/popup'));

  let buildCssForContent = gulp.src([
    'app/css/content/*.css'
    ])
  .pipe(gulp.dest('dist/css/content'));

  let buildFonts = gulp.src('app/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'));

  let buildJs = gulp.src('app/js/**/*')
   .pipe(gulp.dest('dist/js'));

  let buildHtml = gulp.src('app/*.html')
   .pipe(gulp.dest('dist'));

  let buildJson = gulp.src('app/*.json')
    .pipe(gulp.dest('dist'));

});
